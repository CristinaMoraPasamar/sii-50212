


#pragma once


#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida
{
public:         
	Esfera esfera;
	
	Raqueta raqueta1;
	Raqueta raqueta2;
	
	float tiempo_bot;
	int accion;
	int fin_bot;
};
