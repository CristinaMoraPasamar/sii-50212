// CristinaMoraPasamar Disparo.h: interface for the Disparo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_
#pragma once


#include "Vector2D.h"


class Disparo
{
	public:

	float radio;

	Vector2D posicion;
	Vector2D velocidad;
	Vector2D aceleracion;
	//Vector2D origen;


	Disparo();
	virtual ~Disparo();
	
	void Dibuja();
	void Mueve(float t);
	void SetPos(float ix,float iy);
