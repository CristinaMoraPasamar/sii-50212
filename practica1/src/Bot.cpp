// CristinaMoraBot.cpp: implementation of the Bot class.
//
//////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <ctype.h>
#include <cstring>


int main()
{

 DatosMemCompartida *memcomp;

 int fd2=open("BotFIFO", O_RDWR);
 
 if (fd2==-1)
{
 perror("ERROR");
 exit(1);
}
 
 memcomp= (DatosMemCompartida*)  mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0);

 while(1)
  {
	
	//Movimiento raqueta 1
		
			if(memcomp->raqueta1.y1 < memcomp->esfera.centro.y)memcomp->accion=1;
			else if(memcomp->raqueta1.y2 > memcomp->esfera.centro.y)memcomp->accion=-1;
		
		
			if(memcomp->raqueta1.y1 < memcomp->esfera.centro.y)memcomp->accion=1;
			else if(memcomp->raqueta1.y2 > memcomp->esfera.centro.y)memcomp->accion=-1;
		

		//Movimiento raqueta 2
		if((memcomp->tiempo_bot>=10.0f)){
			if(memcomp->raqueta2.y1 < memcomp->esfera.centro.y)memcomp->accion=2;
			else if(memcomp->raqueta2.y2 > memcomp->esfera.centro.y)memcomp->accion=-2;
		}
		else if((memcomp->tiempo_bot>=10.0f)){
			if(memcomp->raqueta2.y1 < memcomp->esfera.centro.y) memcomp->accion=2;
			else if(memcomp->raqueta2.y2 > memcomp->esfera.centro.y) memcomp->accion=-2;
		}
		

  }

        close(fd2);
	unlink("BotFIFO");
	munmap(memcomp,sizeof(DatosMemCompartida));
	return 0;
}

 
