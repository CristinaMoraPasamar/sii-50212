// CristinaMoraDisparo.cpp: implementation of the Disparo class.
//
//////////////////////////////////////////////////////////////////////
#include "Disparo.h"
#include "glut.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
Disparo::Disparo(void)
{
	velocidad.y=0.0F;
	velocidad.x=0.0F;
	radio=0.25f;
	//posicion.x=0.0F;
	//posicion.y=0.0F;
}

Disparo::~Disparo(void)
{
}



void Disparo::Mueve(float t)
{
	
	posicion=posicion+velocidad*t;
	
}

void Disparo::SetPos(float ix,float iy)
{
	
	posicion.x=ix;
	posicion.y=iy;
	//origen.y=posicion.y+1;
	//origen.x=ix;
}
