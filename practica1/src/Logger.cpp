// CristinaMoraLogger.cpp: implementation of the Logger class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <cstring>

int main()
{

  if(mkfifo("LoggerFIFO",0666)!=0){
	perror("Error");
	return 1;
	}

	int fd=open("LoggerFIFO",O_RDONLY);
	if(fd==-1){
	perror("ERROR");
	exit(1);
	}

  while(1){
   char cadFIFO[200];
		int n;
		n=read(fd, cadFIFO, sizeof(cadFIFO));
		if(n==0) break;
		printf("%s\n",cadFIFO);	
}

   close(fd);
	unlink("LoggerFIFO");
	return 0;
}

 
