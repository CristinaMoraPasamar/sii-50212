// CristinaMoraMundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{    
        close(fd);
        close(fd2);
        unlink("PongFIFO");
        unlink("BotFIFO");

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

        

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

       char cadFIFO[400];
       float t_bot;


void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
        t_bot+=0.025f;
	int i,c,d;
     
//para que finalice cuando uno de los dos jugadores logre los 3 puntos

        if(puntos1==3)
{
           printf("\n\t\t\t\tFINAL DEL JUEGO: El ganador es el jugador 1 \n\n\n");
           memcomp ->fin_bot = 1;
           exit(0);
} 

        else if(puntos2==3)
{
           printf("\n\t\t\t\tFINAL DEL JUEGO: El ganador es el jugador 2 \n\n\n");
           memcomp ->fin_bot = 1;
           exit(0);
}

	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

        
	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
        

        if (jugador1.Rebota(esfera))

              c=1;


	if(c>=1 && c<10000)
        {
             
                jugador2.velocidad.y=0.0f;
               c++;
        }


        if (jugador2.Rebota(esfera))

              d=1;


	if(d>=1 && d<10000)
        {
             
               jugador1.velocidad.y=0.0f;
               d++;
        }
             
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

          sprintf(cadFIFO, "El jugador 2 marca 1 punto, hace un total de %d puntos \n", puntos2);
          write(fd, cadFIFO, sizeof(cadFIFO));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

          sprintf(cadFIFO, "El jugador 1 marca 1 punto, hace un total de %d puntos \n", puntos1);
          write(fd, cadFIFO, sizeof(cadFIFO));
	}


        memcomp -> raqueta1 = jugador1;
        memcomp -> raqueta2 = jugador2;
        memcomp -> esfera = esfera;
        memcomp -> tiempo_bot = t_bot;

        if (memcomp -> accion == 1) OnKeyboardDown ('w' , 0,0);
        else if (memcomp -> accion == -1) OnKeyboardDown ('s' , 0,0);
        else if (memcomp -> accion == 2) OnKeyboardDown ('y' , 0,0);
        else if (memcomp -> accion == -2) OnKeyboardDown ('h' , 0,0);
        else if (memcomp -> accion == 3) OnKeyboardDown ('d' , 0,0);
        else if (memcomp -> accion == -3) OnKeyboardDown ('g' , 0,0);
     

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4; t_bot=0.0f;break;
	case 'o':jugador2.velocidad.y=4; t_bot= 0.0f;break;
        case 'h':jugador2.velocidad.y=-4;break;
        case 'y': jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

        puntos1=0;
        puntos2=0;

        t_bot==0.0f;

        fd=open("PongFIFO", O_WRONLY);
        if(fd==-1)
{
         perror("ERROR");
         exit(1);
}

        fd2=open("BotFIFO", O_CREAT|O_RDWR, 0666);
        if(fd2==-1)
{
         perror("ERROR");
         exit(1);
}

       write (fd2, &datosmemcomp, sizeof(datosmemcomp));
       memcomp = (DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0); //proyección del archivo en memoria
}




